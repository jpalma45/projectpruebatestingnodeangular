const saludar = require('../app');

var x = false;
var a = {};
var b = {};

describe('verifica que la variable es true', () => {
it('La función saluda', () => {
  expect(saludar('Platzi')).toBe('Hola Platzi');
});

it('X es true', () => {
  expect(x).toBe(false);
  expect(x).toEqual(false);

});

it('objetos iguales', () => {
  expect(a).toEqual(b);
})
})
